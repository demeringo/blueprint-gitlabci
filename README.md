# blueprint-gitlabci

Basic demo gitlab ci to build a docker image and push it to the docker registry of the project.

## Usage

Check content of docker registry: `registry.gitlab.com/demeringo/blueprint-gitlabci`

Commit something to the repository (or update content of hello.txt) and see gitlab re-building you image.

Run the image locally:
```bash
docker run -rm registry.gitlab.com/demeringo/blueprint-gitlabci:latest
```


## Details

Uses Kaniko (https://github.com/GoogleContainerTools/kaniko) to build the docker image and avoid _docker in docker_.

See https://docs.gitlab.com/ee/ci/docker/using_kaniko.html

